﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.RemoteImageControl.Contract;
using Lab7.AddressControl.Contract;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using System.Net;
using TinyMVVMHelper;


namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImageClass : IRemoteImage
    {
        class XYZ : ViewModel
        {
            private ImageSource zrodloObrazka;
            public ImageSource ZrodloObrazka
            {
                get
                {
                    return zrodloObrazka;
                }

                set
                {
                    zrodloObrazka = value;
                    FirePropertyChanged("ZrodloObrazka");
                }
            }
        }

        XYZ nazwa;
        public Control window;

        Control IRemoteImage.Control
        {
            get
            {
                return window;

            }
        }
        private IAddress adres;
        public RemoteImageClass(IAddress adres)
        {
            window = new RemoteImageControl();
            this.adres = adres;
            nazwa = new XYZ();
            adres.AddressChanged += OnAddressChanged;
            window.DataContext = nazwa;
        }

        public void Load(string url)
        {
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            nazwa.ZrodloObrazka = (ImageSource)imageSourceConverter.ConvertFrom(ms);
        }

        public void OnAddressChanged(object sender, AddressChangedArgs args)
        {
            Load(args.URL);
        }
    }

}
