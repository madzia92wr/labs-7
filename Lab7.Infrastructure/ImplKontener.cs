﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Lab7.Infrastructure
{
    public class ImplKontener : IContainer
    {

        public void Register(Assembly assembly)
        {
            var typy = assembly.GetTypes(); //pobiera typy zdefiniowane w assembly
            for (int i = 0; i < typy.Length; i++)
            {
                if (!(typy[i].IsNotPublic))
                {
                    this.Register(typy[i]);
                }
            }
        }

        public Dictionary<Type, Type> zbior = new Dictionary<Type, Type>();
        public void Register(Type type)
        {   //interfejsy wymagane
            //getInterfaces  pobiera wszystkie interfejsy implementowane lub dziedziczone przez bieżący Type
            //sprawdzenie czy dane interfejsy znajdują się w zbiorze(dictionary) jeżeli nie to zostają one dodane
            var interfejsy = type.GetInterfaces();
            for (int i = 0; i < interfejsy.Length; i++)
            {
                if (!(zbior.ContainsKey(interfejsy[i])))
                {
                    zbior.Add(interfejsy[i], type);
                }
            }
        }

        private Dictionary<Type, Object> obiektiinterfejs = new Dictionary<Type, Object>();
        //przypisanie do konkretnego obiektu konkretnych interfejsów
        //interfejsy dostarczane
        public void Register<T>(T impl) where T : class
        {
            var inte = impl.GetType().GetInterfaces();
            for (int i = 0; i < inte.Length; i++)
            {
                if (!obiektiinterfejs.ContainsKey(inte[i]))
                {
                    obiektiinterfejs.Add(inte[i], impl);
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class //rejestrują interfejsy dostarczane
        {
            var result = provider.Invoke();
            Register(result);
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }


        public object Resolve(Type type)
        {
            if (obiektiinterfejs.ContainsKey(type))
            {
                return obiektiinterfejs[type];
            }
            //zwrócenie klasy pasującej do interfejsu
            if (!zbior.ContainsKey(type))
            {
                return null;
            }

            else
            {
                var konstruktory = zbior[type].GetConstructors(); //Konstruktory obiektu, który implementuje ten interfejs


                for (int i = 0; i < konstruktory.Length; i++)
                {

                    var informacje = konstruktory[i].GetParameters();

                    if (informacje.Length == 0)
                    {
                        return Activator.CreateInstance(zbior[type]);//jeśli ctor nie ma parametrów to zwraca nowy obiekt.
                    }

                    List<object> lista = new List<object>(informacje.Length);

                    for (int j = 0; j < informacje.Length; j++)
                    {


                        var p = Resolve(informacje[j].ParameterType);

                        if (p == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        lista.Add(p);
                    }

                    return konstruktory[i].Invoke(lista.ToArray());
                }
            }

            return null;
        }
    }
}
