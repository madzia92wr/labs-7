﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.AddressControl.Contract;
using System.Windows.Controls;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
   public class AddressContolClass :IAddress
    {
       public event EventHandler<AddressChangedArgs> AddressChanged;

       private Command command;
       public Command Command
       {
           get { return command; }
           set { command = value; }
       }

       private string adres;
       public string Adres
       {
           get { return adres; }
           set { adres = value; }
       }
       private Control window;

       Control IAddress.Control
       {
           get { return window; }
       }

       public AddressContolClass()
       {
           window = new AddressControl();
           this.Command = new Command(FireAdressChanged);
       }

       private void FireAdressChanged(object obj)
       {
           if (AddressChanged != null)
           {
               AddressChangedArgs arg = new AddressChangedArgs();
               arg.URL = Adres;
               AddressChanged(this, arg);
           }
       }
        
    }
}
